package pl.codementors.partex.view.category;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Category;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Bean for displaying categories.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class CategoriesList implements Serializable {

    @EJB
    private DataStore store;

    /**
     * List of categories to be displayed.
     */
    private List<Category> categories;

    /**
     *
     * @return All categories from the database.
     */
    public List<Category> getCategories() {
        if (categories == null) {
            categories = store.getCategories();
        }
        return categories;
    }

    /**
     * Deletes single category from view, but first checks if it is in use.
     * @param category Category to by deleted.
     */
    public void delete(Category category) {
        if(store.canBeDeleted(category)) {
            store.deleteCategory(category);
            categories = null;
        }
    }
}
