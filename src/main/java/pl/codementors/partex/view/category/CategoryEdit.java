package pl.codementors.partex.view.category;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Category;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean for edition and addition of single category.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class CategoryEdit implements Serializable {

    private static final Logger log = Logger.getLogger(CategoryEdit.class.getName());

    @EJB
    private DataStore store;

    /**
     * Displayed category (in edition).
     */
    private Category category;

    /**
     * Id of selected category fetched form query params.
     */
    private int categoryId;

    /**
     * Displayed category (in addition).
     */
    private Category newCategory = new Category();

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public Category getCategory() {
        if (category == null) {
            category = store.getCategory(categoryId);
        }
        return category;
    }

    public Category getNewCategory() {
        return newCategory;
    }

    public void setNewCategory(Category newCategory) {
        this.newCategory = newCategory;
    }

    /**
     * Saves edited category. Checks if the name already exists in database.
     */
    public void saveCategory() {
        if (store.nameNotExist(category)) {
            store.updateCategory(category);
            try {
                throw new Exception("Nazwa kategorii została poprawnie zmieniona. (Category name has been changed properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Ta nazwa kategorii jest już zajęta! (This category name is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
    }

    /**
     * Adds new category. Checks if the name already exists in database.
     */
    public void addCategory() {
        if (store.nameNotExist(newCategory)) {
            store.createCategory(newCategory);
            newCategory = new Category();
            try {
                throw new Exception("Nowa kategoria została dodana poprawnie. (New category has been added properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Ta nazwa kategorii jest już zajęta! (This category name is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
    }
}
