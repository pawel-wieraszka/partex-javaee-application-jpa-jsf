package pl.codementors.partex.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

/**
 * Bean for checking role of user and to close session.
 *
 * @author Paweł Wieraszka
 */
@ManagedBean
@SessionScoped
public class RoleView implements Serializable {

    /**
     *
     * @return True if user is in role ADMIN.
     */
    public boolean isAdmin() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN");
    }

    /**
     *
     * @return True if user is in role USER.
     */
    public boolean isUser() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("USER");
    }

    /**
     * Logs user out with session invalidation.
     * @return Redirection command.
     */
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/item/items_list.xhtml?faces-redirect=true";
    }
}
