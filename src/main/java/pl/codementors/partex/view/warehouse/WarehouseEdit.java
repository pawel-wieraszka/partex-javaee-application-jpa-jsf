package pl.codementors.partex.view.warehouse;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Warehouse;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean for edition and addition of single warehouse.
 *
 * @author gigant0502
 */
@Named
@ViewScoped
public class WarehouseEdit implements Serializable {

    private static final Logger log = Logger.getLogger(WarehouseEdit.class.getName());

    @EJB
    private DataStore store;

    /**
     * Displayed warehouse (in edition).
     */
    private Warehouse warehouse;

    /**
     * Id of selected warehouse fetched form query params.
     */
    private int warehouseId;

    /**
     * Displayed warehouse (in addition).
     */
    private Warehouse newWarehouse = new Warehouse();

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Warehouse getWarehouse() {
        if (warehouse == null) {
            warehouse = store.getWarehouse(warehouseId);
        }
        return warehouse;
    }

    public Warehouse getNewWarehouse() {
        return newWarehouse;
    }

    public void setNewWarehouse(Warehouse newWarehouse) {
        this.newWarehouse = newWarehouse;
    }

    /**
     * Saves edited warehouse. Checks if the name already exists in database.
     */
    public void saveWarehouse() {
        if (store.nameNotExist(warehouse)) {
            store.updateWarehouse(warehouse);
            try {
                throw new Exception("Nazwa magazynu została poprawnie zmieniona. (Warehouse name has been changed properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Ta nazwa magazynu jest już zajęta! (This warehouse name is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
    }

    /**
     * Adds new warehouse. Checks if the name already exists in database.
     */
    public void addWarehouse() {
        if (store.nameNotExist(newWarehouse)) {
            store.createWarehouse(newWarehouse);
            newWarehouse = new Warehouse();
            try {
                throw new Exception("Nowy magazyn został dodany poprawnie. (New warehouse has been added properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Ta nazwa magazynu jest już zajęta! (This warehouse name is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
    }
}
