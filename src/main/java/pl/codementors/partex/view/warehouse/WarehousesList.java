package pl.codementors.partex.view.warehouse;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Warehouse;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Bean for displaying Warehouses.
 *
 * @author gigant0502
 */
@Named
@ViewScoped
public class WarehousesList implements Serializable {

    @EJB
    private DataStore store;

    /**
     * List of warehouses to be displayed.
     */
    private List<Warehouse> warehouses;

    /**
     *
     * @return All warehouses from the database.
     */
    public List<Warehouse> getWarehouses() {
        if (warehouses == null) {
            warehouses = store.getWarehouses();
        }
        return warehouses;
    }

    /**
     * Deletes single warehouse from view, but first checks if it is in use.
     * @param warehouse Warehouse to be deleted.
     */
    public void delete(Warehouse warehouse) {
        if (store.canBeDeleted(warehouse)) {
            store.deleteWarehouse(warehouse);
            warehouses = null;
        }
    }
}
