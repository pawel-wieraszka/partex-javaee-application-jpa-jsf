package pl.codementors.partex.view.department;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Department;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Bean for displaying departments.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class DepartmentsList implements Serializable {

    @EJB
    private DataStore store;

    /**
     * List of departments do be displayed.
     */
    private List<Department> departments;

    /**
     *
     * @return All departments form the database.
     */
    public List<Department> getDepartments() {
        if (departments == null) {
            departments = store.getDepartments();
        }
        return departments;
    }

    /**
     * Deletes single department from view, but first checks if it is in use.
     * @param department Department to be deleted.
     */
    public void delete(Department department) {
        if (store.canBeDeleted(department)) {
            store.deleteDepartment(department);
            departments = null;
        }
    }
}
