package pl.codementors.partex.view.department;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Department;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean for edition and addition of single department.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class DepartmentEdit implements Serializable {

    private static final Logger log = Logger.getLogger(DepartmentEdit.class.getName());

    @EJB
    private DataStore store;

    /**
     * Displayed department (in edition).
     */
    private Department department;

    /**
     * Id of selected department fetched form query params.
     */
    private int departmentId;

    /**
     * Displayed department (in addition).
     */
    private Department newDepartment = new Department();

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public Department getDepartment() {
        if (department == null) {
            department = store.getDepartment(departmentId);
        }
        return department;
    }

    public Department getNewDepartment() {
        return newDepartment;
    }

    public void setNewDepartment(Department newDepartment) {
        this.newDepartment = newDepartment;
    }

    /**
     * Saves edited department. Checks if the name already exists in database.
     */
    public void saveDepartment() {
        if (store.nameNotExist(department)) {
            store.updateDepartment(department);
            try {
                throw new Exception("Nazwa działu została poprawnie zmieniona. (Department name has been changed properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Ta nazwa działu jest już zajęta! (This department name is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
    }

    /**
     * Adds new department. Checks if the name already exists in database.
     */
    public void addDepartment() {
        if (store.nameNotExist(newDepartment)) {
            store.createDepartment(newDepartment);
            newDepartment = new Department();
            try {
                throw new Exception("Nowy dział został dodany poprawnie. (New department has been added properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Ta nazwa działu jest już zajęta! (This department name is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
    }
}
