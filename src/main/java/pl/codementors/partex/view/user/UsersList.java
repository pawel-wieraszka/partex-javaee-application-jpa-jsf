package pl.codementors.partex.view.user;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.User;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Bean for displaying users.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class UsersList implements Serializable {

    @EJB
    private DataStore store;

    /**
     * List of users to be displayed
     */
    private List<User> users;

    /**
     *
     * @return All users from the database.
     */
    public List<User> getUsers() {
        if (users == null) {
            users = store.getUsers();
        }
        return users;
    }

    /**
     * Deletes single user from view.
     * @param user User to be deleted.
     */
    public void delete(User user) {
        store.deleteUser(user);
        users = null;
    }
}
