package pl.codementors.partex.view.user;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.User;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean for edition and addition of single user.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class UserEdit implements Serializable {

    private static final Logger log = Logger.getLogger(UserEdit.class.getName());

    @EJB
    private DataStore store;

    /**
     * Displayed user (in edition).
     */
    private User user;

    /**
     * Id of selected user fetched form query params.
     */
    private int userId;

    /**
     * Displayed user (in addition).
     */
    private User newUser = new User();

    /**
     * List of roles to choose in select one listbox.
     */
    private List<SelectItem> roles;

    /**
     * List of roles imported from enum.
     */
    private List<User.Role> rolesToChoose = Arrays.asList(User.Role.values());

    /**
     *
     * @return All available roles from enum.
     */
    public List<SelectItem> getRoles() {
        if (roles == null) {
            roles = new ArrayList<SelectItem>();
            rolesToChoose.forEach(role -> {
                roles.add(new SelectItem(role, role + ""));
            });
        }
        return roles;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        if (user == null) {
            user = store.getUser(userId);
        }
        return user;
    }

    public User getNewUser() {
        return newUser;
    }

    public void setNewUser(User newUser) {
        this.newUser = newUser;
    }

    /**
     * Saves edited user.
     */
    public void saveUser() {
        store.updateUser(user);
        try {
            throw new Exception("Dane użytkownika zostały poprawnie zmienione. (User data has been changed properly.)");
        } catch (Exception ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage("form:login", new FacesMessage(ex.getMessage()));
        }
    }

    /**
     * Adds new user. Checks if login already exists.
     */
    public void addUser() {
        if (store.loginNotExist(newUser)) {
            store.createUser(newUser);
            newUser = new User();
            try {
                throw new Exception("Nowy użytkownik został poprawnie dodany. (New user has been added properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:login", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Ten login jest już zajęty! (This login is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:login", new FacesMessage(ex.getMessage()));
            }
        }
    }
}
