package pl.codementors.partex.view.tag;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Tag;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean for edition and addition of single tag.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class TagEdit implements Serializable {

    private static final Logger log = Logger.getLogger(TagEdit.class.getName());

    @EJB
    private DataStore store;

    /**
     * Displayed tag (in edition).
     */
    private Tag tag;

    /**
     * Id of selected tag fetched form query params.
     */
    private int tagId;

    /**
     * Displayed tag (in addition).
     */
    private Tag newTag = new Tag();

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public Tag getTag() {
        if (tag == null) {
            tag = store.getTag(tagId);
        }
        return tag;
    }

    public Tag getNewTag() {
        return newTag;
    }

    public void setNewTag(Tag newTag) {
        this.newTag = newTag;
    }

    /**
     * Saves edited tag. Checks if the name already exists in database.
     */
    public void saveTag() {
        if (store.nameNotExist(tag)) {
            store.updateTag(tag);
            try {
                throw new Exception("Nazwa tagu została poprawnie zmieniona. (Tag name has been changed properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Taki tag już istnieje w bazie danych! (This tag name is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
    }

    /**
     * Adds new tag. Checks if the name already exists in database.
     */
    public void addTag() {
        if (store.nameNotExist(newTag)) {
            store.createTag(newTag);
            newTag = new Tag();
            try {
                throw new Exception("Nowy tag został dodany poprawnie. (New tag has been added properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Taki tag już istnieje w bazie danych! (This tag name is already in use!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:name", new FacesMessage(ex.getMessage()));
            }
        }
    }
}
