package pl.codementors.partex.view.tag;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Tag;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean for displaying tags.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class TagsList implements Serializable {

    @EJB
    private DataStore store;

    /**
     * List of tags to be displayed.
     */
    private List<Tag> tags;

    /**
     *
     * @return All tags from the database.
     */
    public List<Tag> getTags() {
        if (tags == null) {
            tags = new ArrayList<>();
            tags = store.getTags();
        }
        return tags;
    }

    /**
     * Deletes single tag from view, but first checks if it is in use.
     * @param tag Tag to be deleted.
     */
    public void delete(Tag tag) {
        if (store.canBeDeleted(tag)) {
           store.deleteTag(tag);
           tags = null;
        }
    }
}
