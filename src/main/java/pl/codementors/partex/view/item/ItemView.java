package pl.codementors.partex.view.item;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Item;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.*;

/**
 * Bean for displaying info about single item.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class ItemView implements Serializable {

    @EJB
    private DataStore store;

    /**
     * Id of selected item fetched from query params.
     */
    private int itemId;

    /**
     * Displayed item.
     */
    private Item item;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public Item getItem() {
        if (item == null) {
            item = store.getItem(itemId);
        }
        return item;
    }
}
