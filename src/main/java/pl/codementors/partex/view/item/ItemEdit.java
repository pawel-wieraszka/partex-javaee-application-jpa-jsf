package pl.codementors.partex.view.item;

import org.apache.commons.io.IOUtils;
import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Item;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

/**
 * Bean for edition and addition of single item.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class ItemEdit implements Serializable {

    private static final Logger log = Logger.getLogger(ItemEdit.class.getName());

    @EJB
    private DataStore store;

    /**
     * Id of selected item fetched form query params.
     */
    private int itemId;

    /**
     * Displayed item (in edition).
     */
    private Item item;

    /**
     * Displayed item (in addition).
     */
    private Item newItem = new Item();

    /**
     * Photo file to be saved.
     */
    private Part file;

    /**
     * Name of photo file to be saved.
     */
    private String fotoName;

    /**
     * List of tags available in select many checkbox.
     */
    private List<SelectItem> tags;

    /**
     * List of departments available in select one listbox.
     */
    private List<SelectItem> departments;

    /**
     * List of categories available in select one listbox.
     */
    private List<SelectItem> categories;

    /**
     * List of warehouses available in select one listbox.
     */
    private List<SelectItem> warehouses;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public Item getItem() {
        if (item == null) {
            item = store.getItem(itemId);
        }
        return item;
    }

    public Item getNewItem() {
        return newItem;
    }

    public void setNewItem(Item item) {
        this.newItem = item;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public String getFotoName() {
        return fotoName;
    }

    public void setFotoName(String fotoName) {
        this.fotoName = fotoName;
    }

    /**
     *
     * @return List of available tags.
     */
    public List<SelectItem> getTags() {
        if (tags == null) {
            tags = new ArrayList<SelectItem>();
            store.getTags().forEach(tag -> {
                tags.add(new SelectItem(tag, tag.getName() + ""));
            });
        }
        return tags;
    }

    /**
     *
     * @return List of available categories.
     */
    public List<SelectItem> getCategories() {
        if (categories == null) {
            categories = new ArrayList<SelectItem>();
            categories.add(new SelectItem(null, "---"));
            store.getCategories().forEach(category -> {
                categories.add(new SelectItem(category,category.getName() + ""));
            });
        }
        return categories;
    }

    /**
     *
     * @return List of available warehouses.
     */
    public List<SelectItem> getWarehouses() {
        if (warehouses == null) {
            warehouses = new ArrayList<SelectItem>();
            warehouses.add(new SelectItem(null, "---"));
            store.getWarehouses().forEach(warehouse -> {
                warehouses.add(new SelectItem(warehouse,warehouse.getName() + ""));
            });
        }
        return warehouses;
    }

    /**
     *
     * @return List of available departments.
     */
    public List<SelectItem> getDepartments() {
        if (departments == null) {
            departments = new ArrayList<SelectItem>();
            departments.add(new SelectItem(null, "---"));
            store.getDepartments().forEach(department -> {
                departments.add(new SelectItem(department,department.getName() + ""));
            });
        }
        return departments;
    }

    /**
     * Saves edited item.
     */
    public void saveItem() {
        store.updateItem(item);
    }

    /**
     *
     * @return Path to upload from config.properties file.
     */
    private String getPath() {
        Properties prop = new Properties();
        InputStream input;
        try {
            String file = "pl/codementors/partex/config/config.properties";
            input = ItemEdit.class.getClassLoader().getResourceAsStream(file);
            prop.load(input);
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return prop.getProperty("path");
    }

    /**
     * Uploads a photo file and saves it on local hard drive.
     */
    private void upload() {
        String path = getPath();
        try (InputStream in = file.getInputStream();
             FileOutputStream out = new FileOutputStream( path + fotoName)) {
            IOUtils.copy(in, out);
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    /**
     * Sends a form with data needed to create a new item.
     * Sets date to now. Firstly checks if photo file was chosen.
     */
    public void addItem() {
        if (file != null) {
            store.createItem(newItem);
            setFotoName("" + newItem.getId());
            newItem.setPhotoName(fotoName);
            store.updateItem(newItem);
            upload();
            newItem = new Item();
            try {
                throw new Exception("Przedmiot został poprawnie dodany. (Item has been added properly.)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:file", new FacesMessage(ex.getMessage()));
            }
        }
        else {
            try {
                throw new Exception("Nie wybrałeś zdjęcia! (You didn't choose any photo file!)");
            } catch (Exception ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                FacesContext.getCurrentInstance().addMessage("form:file", new FacesMessage(ex.getMessage()));
            }
        }
    }
}
