package pl.codementors.partex.view.item;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Item;
import pl.codementors.partex.model.Tag;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Bean for displaying items.
 *
 * @author Paweł Wieraszka
 */
@Named
@ViewScoped
public class ItemsList implements Serializable {

    @EJB
    private DataStore store;

    /**
     * List of items to be displayed.
     */
    private List<Item> items;

    /**
     * List of tags to be displayed.
     */
    private List<Tag> tags;

    /**
     *
     * @return All available items.
     */
    public List<Item> getItems() {
        if (items == null) {
            items = store.getItems();
        }
        return items;
    }

    /**
     *
     * @return All available tags.
     */
    public List<Tag> getTags() {
        if (tags == null) {
            tags = store.getTags();
        }
        return tags;
    }

    /**
     * Filters items by chosen tag.
     * @param tag Tag with which items are going to be filtered.
     */
    public void filterByTag(Tag tag) {
        items = store.getItemsWithTag(tag);
    }

    /**
     *Undoing chosen tag filter.
     */
    public void clearTags() {
        items = store.getItems();
    }

    /**
     * Called from view. Deletes single item.
     * @param item Item to be deleted.
     */
    public void delete(Item item) {
        store.deleteItem(item);
        items = null;
    }

    /**
     * Deletes all items chosen in checkbox.
     */
    public void deleteItems(){
        for(Item item : items){
            if(item.isDelete()){
                store.deleteItem(item);
            }
        }
        items = null;
    }
}
