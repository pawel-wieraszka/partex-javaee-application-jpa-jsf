package pl.codementors.partex.view.converter;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Tag;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Converter for Tag class.
 */
@FacesConverter("tagConverter")
public class TagConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        DataStore store = CDI.current().select(DataStore.class).get();
        if (value == null || value.equals("null")) {
            return null;
        }
        return store.getTag(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "null";
        }
        return ((Tag) value).getId() + "";
    }
}
