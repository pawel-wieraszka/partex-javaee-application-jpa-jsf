package pl.codementors.partex.view.converter;

import pl.codementors.partex.DataStore;
import pl.codementors.partex.model.Warehouse;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Converter for Warehouse class.
 */
@FacesConverter("warConverter")
public class WarConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        DataStore store = CDI.current().select(DataStore.class).get();
        if (value == null || value.equals("null")) {
            return null;
        }
        return store.getWarehouse(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "null";
        }
        return ((Warehouse) value).getId() + "";
    }
}
