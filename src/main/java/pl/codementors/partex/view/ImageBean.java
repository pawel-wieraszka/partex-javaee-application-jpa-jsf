package pl.codementors.partex.view;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import pl.codementors.partex.DataStore;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean for displaying image of item.
 *
 * @author Paweł Wieraszka
 */
@ManagedBean
public class ImageBean implements Serializable {

    private static final Logger log = Logger.getLogger(DataStore.class.getName());

    /**
     *
     * @return Path to folder with images from config.properties file.
     */
    private String getPath() {
        Properties prop = new Properties();
        InputStream input;
        try {
            String file = "pl/codementors/partex/config/config.properties";
            input = ImageBean.class.getClassLoader().getResourceAsStream(file);
            prop.load(input);
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return prop.getProperty("path");
    }

    /**
     * In first phase of page loading returns fake stream content. In second phase,
     * when browser is looking for picture, returns right image. Reads param id
     * of displaying item from ItemView bean.
     * @return Individual image for item.
     * @throws IOException
     */
    public StreamedContent getContent() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        }
        else {
            String param = context.getExternalContext().getRequestParameterMap().get("id");
            File file = new File(getPath(), "" + param);
            return new DefaultStreamedContent(new FileInputStream(file));
        }
    }
}
