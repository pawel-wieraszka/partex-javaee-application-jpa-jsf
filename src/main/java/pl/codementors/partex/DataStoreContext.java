package pl.codementors.partex;

import pl.codementors.partex.model.*;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Responsible for all activities performed when the application is launched.
 *
 * @author Paweł Wieraszka
 */
@Singleton
@Startup
public class DataStoreContext {

    private static final Logger log = Logger.getLogger(DataStoreContext.class.getName());

    @PersistenceContext
    private EntityManager em;

    /**
     * When the application is started, create a super user account (admin).
     */
    @PostConstruct
    public void init() {

        User admin = new User("admin", "admin", "admin", "admin", User.Role.ADMIN);
        em.persist(admin);

//        to delete:
        User user = new User("user", "user", "user", "user", User.Role.USER);
        em.persist(user);

        Category category1 = new Category("category1");
        em.persist(category1);

        Category category2 = new Category();
        category2.setName("category2");
        em.persist(category2);

        Category category3 = new Category();
        category3.setName("category3");
        em.persist(category3);

        Category category4 = new Category();
        category4.setName("category4");
        em.persist(category4);

        Department department1 = new Department("department1");
        em.persist(department1);

        Department department2 = new Department();
        department2.setName("department2");
        em.persist(department2);

        Department department3 = new Department();
        department3.setName("department3");
        em.persist(department3);

        Tag tag1 = new Tag("tag1");
        em.persist(tag1);

        Tag tag2 = new Tag();
        tag2.setName("tag2");
        em.persist(tag2);

        Tag tag3 = new Tag("tag3");
        em.persist(tag3);

        Tag tag4 = new Tag("tag4");
        em.persist(tag4);

        Warehouse warehouse1 = new Warehouse("warehouse1");
        em.persist(warehouse1);

        Warehouse warehouse2 = new Warehouse();
        warehouse2.setName("warehouse2");
        em.persist(warehouse2);

        Warehouse warehouse3 = new Warehouse("warehouse3");
        em.persist(warehouse3);

        User user1 = new User("login", "name", "surname", "password", User.Role.USER);
        em.persist(user1);

        User user2 = new User();
        user2.setLogin("login2");
        user2.setName("name2");
        user2.setSurname("surname2");
        user2.setPassword("password2");
        user2.setRole(User.Role.ADMIN);
        em.persist(user2);

        Item item1 = new Item("part", warehouse1, department1, category1, Arrays.asList(tag1,tag2), "asd");
        item1.setPhotoName("1");
        em.persist(item1);

        Item item2 = new Item();
        item2.setName("part2");
        item2.setCategory(category2);
        item2.setDepartment(department2);
        item2.setWarehouse(warehouse2);
        item2.setTags(Arrays.asList(tag3,tag2));
        item2.setDescription("asdf");
        item2.setPhotoName("2");
        em.persist(item2);

        Item item3 = new Item("part3", warehouse1, department2, category3, Arrays.asList(tag1,tag2,tag3), "asd");
        item3.setPhotoName("3");
        em.persist(item3);

        Item item4 = new Item("part4", warehouse1, department1, category1, Arrays.asList(tag1,tag3), "asd");
        item4.setPhotoName("4");
        em.persist(item4);
    }
}
