package pl.codementors.partex.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Item class.
 *
 * @author Paweł Wieraszka
 */
@Entity
@Table(name = "items")
public class Item implements Serializable{

    /**
     * Item id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Item name.
     */
    @Column()
    private String name;

    /**
     * date and time when item was added.
     */
    @Column(name = "date")
    private LocalDateTime date = LocalDateTime.now();
    /**
     * Warehouse where the item is lockated.
     */
    @ManyToOne()
    @JoinColumn(name = "warehouse", referencedColumnName = "id")
    private Warehouse warehouse;

    /**
     * Department where the item is lockated.
     */
    @ManyToOne()
    @JoinColumn(name = "department", referencedColumnName = "id")
    private Department department;

    /**
     * Category of the item.
     */
    @ManyToOne()
    @JoinColumn(name = "category", referencedColumnName = "id")
    private Category category;

    /**
     * Tags of the item.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tags_items", joinColumns = @JoinColumn(name = "item", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tag", referencedColumnName = "id"))
    private List<Tag> tags = new ArrayList<>();

    /**
     * Description of the item.
     */
    @Column
    private String description;

    /**
     * Name of photo of the item.
     */
    @Column
    private String photoName;

    /**
     * Item should be deleted or not.
     */
    @Transient
    private boolean delete;

    public Item() {
    }

    public Item(String name, Warehouse warehouse, Department department, Category category, List<Tag> tags, String description) {
        this.name = name;
        this.warehouse = warehouse;
        this.department = department;
        this.category = category;
        this.tags = tags;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                delete == item.delete &&
                Objects.equals(name, item.name) &&
                Objects.equals(date, item.date) &&
                Objects.equals(warehouse, item.warehouse) &&
                Objects.equals(department, item.department) &&
                Objects.equals(category, item.category) &&
                Objects.equals(tags, item.tags) &&
                Objects.equals(description, item.description) &&
                Objects.equals(photoName, item.photoName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, date, warehouse, department, category, tags, description, photoName, delete);
    }
}
