package pl.codementors.partex.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Tag class.
 *
 * @author Paweł Wieraszka
 */
@Entity
@Table(name = "tags")
public class Tag implements Serializable {

    /**
     * Tag id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Tag name.
     */
    @Column(unique = true)
    private String name;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return id == tag.id &&
                Objects.equals(name, tag.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }
}
