package pl.codementors.partex.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Department class.
 *
 * @author Paweł Wieraszka
 */
@Entity
@Table(name = "departments")
public class Department implements Serializable {

    /**
     * Department id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Departmenet name.
     */
    @Column(unique = true)
    private String name;

    public Department() {
    }

    public Department(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return id == that.id &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }
}
