package pl.codementors.partex.model;

import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * User class.
 *
 * @author Paweł Wieraszka
 */
@Entity
@Table(name = "users")
public class User implements Serializable {

    /**
     * Enum role of user declaration.
     */
    public enum Role {

        ADMIN,
        USER;
    }

    /**
     * User id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * User login.
     */
    @Column(unique = true)
    private String login;

    /**
     * User name.
     */
    @Column
    private String name;

    /**
     * User surname.
     */
    @Column
    private String surname;

    /**
     * User password.
     */
    @Column
    private String password;

    /**
     * Type of user.
     */
    @Column
    @Enumerated(value = EnumType.STRING)
    private Role role;

    public User() {
    }

    public User(String login, String name, String surname, String password, Role role) {
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.password = DigestUtils.sha256Hex(password);
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = DigestUtils.sha256Hex(password);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(login, user.login) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(password, user.password) &&
                role == user.role;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, login, name, surname, password, role);
    }
}
