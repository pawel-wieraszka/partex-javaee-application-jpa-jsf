package pl.codementors.partex.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Warehouse class.
 *
 * @author Paweł Wieraszka
 */
@Entity
@Table(name = "warehouses")
public class Warehouse implements Serializable {

    /**
     * Warehouse id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Warehouse name.
     */
    @Column(unique = true)
    private String name;

    public Warehouse() {
    }

    public Warehouse(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Warehouse warehouse = (Warehouse) o;
        return id == warehouse.id &&
                Objects.equals(name, warehouse.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }
}
