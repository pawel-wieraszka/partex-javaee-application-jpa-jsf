package pl.codementors.partex;

import pl.codementors.partex.model.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Performs queries and operations on the database.
 *
 * @author Paweł Wieraszka
 */
@Stateless
public class DataStore {

    private static final Logger log = Logger.getLogger(DataStore.class.getName());

    @PersistenceContext
    private EntityManager em;

    /**
     *
     * @return All items.
     */
    public List<Item> getItems() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        query.from(Item.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @param tag Tag of items to by returned.
     * @return All items with chosen tag.
     */
    public List<Item> getItemsWithTag(Tag tag) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> root = query.from(Item.class);
        query.select(root).where(cb.isMember(tag, root.get(Item_.tags)));
        return em.createQuery(query).getResultList();
    }

    /**
     * Checks if tag can be deleted.
     * @param tag Tag to be checked if any item contains it.
     * @return True if tag is not in use.
     */
    public Boolean canBeDeleted(Tag tag) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> root = query.from(Item.class);
        query.select(root).where(cb.isMember(tag, root.get(Item_.tags)));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     * Checks if category can be deleted.
     * @param category Category to be checked if any item contains it.
     * @return True if category is not in use.
     */
    public Boolean canBeDeleted(Category category) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> root = query.from(Item.class);
        query.select(root).where(cb.equal(root.get(Item_.category), category));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     * Checks if department can be deleted.
     * @param department Department to be checked if any item contains it.
     * @return True if department is not in use.
     */
    public Boolean canBeDeleted(Department department) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> root = query.from(Item.class);
        query.select(root).where(cb.equal(root.get(Item_.department), department));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     * Checks if warehouse can be deleted.
     * @param warehouse Warehouse to be checked if any item contains it.
     * @return True if warehouse is not in use.
     */
    public Boolean canBeDeleted(Warehouse warehouse) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> root = query.from(Item.class);
        query.select(root).where(cb.equal(root.get(Item_.warehouse), warehouse));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     * Checks if tag name already exists.
     * @param tag Tag to be checked.
     * @return True if tag name doesn't exist.
     */
    public Boolean nameNotExist(Tag tag) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> root = query.from(Tag.class);
        query.select(root).where(cb.equal(root.get(Tag_.name), tag.getName()));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     * Checks if category name already exists.
     * @param category Category to be checked.
     * @return True if category name doesn't exist.
     */
    public Boolean nameNotExist(Category category) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> query = cb.createQuery(Category.class);
        Root<Category> root = query.from(Category.class);
        query.select(root).where(cb.equal(root.get(Category_.name), category.getName()));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     * Checks if department name already exists.
     * @param department Department to be checked.
     * @return True if department name doesn't exist.
     */
    public Boolean nameNotExist(Department department) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Department> query = cb.createQuery(Department.class);
        Root<Department> root = query.from(Department.class);
        query.select(root).where(cb.equal(root.get(Department_.name), department.getName()));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     * Checks if warehouse name already exists.
     * @param warehouse Warehouse to be checked.
     * @return True if warehouse name doesn't exist.
     */
    public Boolean nameNotExist(Warehouse warehouse) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Warehouse> query = cb.createQuery(Warehouse.class);
        Root<Warehouse> root = query.from(Warehouse.class);
        query.select(root).where(cb.equal(root.get(Warehouse_.name), warehouse.getName()));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     * Checks if user login already exists.
     * @param user User to be checked.
     * @return True if user login doesn't exist.
     */
    public Boolean loginNotExist(User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(cb.equal(root.get(User_.login), user.getLogin()));
        return em.createQuery(query).getResultList().size() == 0;
    }

    /**
     *
     * @return all tags.
     */
    public List<Tag> getTags() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        query.from(Tag.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @return All categories.
     */
    public List<Category> getCategories() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> query = cb.createQuery(Category.class);
        query.from(Category.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @return All departments.
     */
    public List<Department> getDepartments() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Department> query = cb.createQuery(Department.class);
        query.from(Department.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @return All warehouses
     */
    public List<Warehouse> getWarehouses() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Warehouse> query = cb.createQuery(Warehouse.class);
        query.from(Warehouse.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @return All users.
     */
    public List<User> getUsers() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        query.from(User.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @param id Id of user to be returned.
     * @return Single user.
     */
    public User getUser(int id) {
        return em.find(User.class, id);
    }

    /**
     *
     * @param id Id of item to be returned.
     * @return Single item.
     */
    public Item getItem(int id) {
        Item item = em.find(Item.class, id);
        em.detach(item);
        item.setTags(new ArrayList<>(item.getTags()));
        return item;
    }

    /**
     *
     * @param id Id of tag to be returned.
     * @return Single tag.
     */
    public Tag getTag(int id) {
        return em.find(Tag.class, id);
    }

    /**
     *
     * @param id Id of department to be returned.
     * @return Single department.
     */
    public Department getDepartment(int id) {
        return em.find(Department.class, id);
    }

    /**
     *
     * @param id Id of category to be returned.
     * @return Single category.
     */
    public Category getCategory(int id) {
        return em.find(Category.class, id);
    }

    /**
     *
     * @param id Id of warehouse to be returned.
     * @return Single warehouse.
     */
    public Warehouse getWarehouse(int id) {
        return em.find(Warehouse.class, id);
    }

    /**
     * Updates existing item.
     * @param item Item to be updated.
     */
    public void updateItem(Item item) {
        em.merge(item);
    }

    /**
     * Creates new item.
     * @param item Item to be created.
     */
    public void createItem(Item item) {
        em.persist(item);
    }

    /**
     * Deletes single item.
     * @param item Item to be deleted.
     */
    public void deleteItem(Item item) {
        em.remove(em.merge(item));
    }

    /**
     * Updates existing warehouse.
     * @param warehouse Warehouse to be updated.
     */
    public void updateWarehouse(Warehouse warehouse) {
        em.merge(warehouse);
    }

    /**
     * Creates new warehouse.
     * @param warehouse Warehouse to be created.
     */
    public void createWarehouse(Warehouse warehouse) {
        em.persist(warehouse);
    }

    /**
     * Deletes single warehouse.
     * @param warehouse Warehouse to be deleted.
     */
    public void deleteWarehouse(Warehouse warehouse) {
        em.remove(em.merge(warehouse));
    }

    /**
     * Updates existing category.
     * @param category Category to be updated.
     */
    public void updateCategory(Category category) {
        em.merge(category);
    }

    /**
     * Creates new category.
     * @param category Category to be created.
     */
    public void createCategory(Category category) {
        em.persist(category);
    }

    /**
     * Deletes single category.
     * @param category Category to be deleted.
     */
    public void deleteCategory(Category category) {
        em.remove(em.merge(category));
    }

    /**
     * Updates existing department.
     * @param department Department to be updated.
     */
    public void updateDepartment(Department department) {
        em.merge(department);
    }

    /**
     * Creates new department.
     * @param department Department to be created.
     */
    public void createDepartment(Department department) {
        em.persist(department);
    }

    /**
     * Deletes single department.
     * @param department Department to be deleted.
     */
    public void deleteDepartment(Department department) {
        em.remove(em.merge(department));
    }

    /**
     * Updates existing tag.
     * @param tag Tag to be updated.
     */
    public void updateTag(Tag tag) {
        em.merge(tag);
    }

    /**
     * Creates new tag.
     * @param tag Tag to be created.
     */
    public void createTag(Tag tag) {
        em.persist(tag);
    }

    /**
     * Deletes single tag.
     * @param tag Tag to be deleted.
     */
    public void deleteTag(Tag tag) {
        em.remove(em.merge(tag));
    }

    /**
     * Updates existing user.
     * @param user User to be updated.
     */
    public void updateUser(User user) {
        em.merge(user);
    }

    /**
     * Creates new user.
     * @param user User to be created.
     */
    public void createUser(User user) {
        em.persist(user);
    }

    /**
     * Deletes single user.
     * @param user User to be deleted.
     */
    public void deleteUser(User user) {
        em.remove(em.merge(user));
    }
}
